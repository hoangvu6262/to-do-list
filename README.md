## 1. Cho 1 mảng gồm các số nguyên. Tìm vị trí của số có giá trị tuyệt đối lớn nhất (mô tả các bước và viết mã giả)

### Bước 1: tạọ biến để lưu vị trí của số có giá trị tuyệt đối lớn nhất, defaut sẽ cho ở vị trí đầu tiên

```bash
# Gán vị trí 0 cho biến largestNumberIndex
const largestNumberIndex = 0

```

### Bước 2: Chạy vòng lặp for để lặp qua một lượt các phần tử trong mảng arr: number[]

```bash
# Duyệt mảng arr : number[]
for (let i = 1; i < arr.length; i++)
```

và kiểm tra nếu có bất kỳ giá trị tuyệt đối của phần tử trong mảng lớn > giá trị tuyệt đối của phần tử largestNumberIndex thì ta sẽ update biến này là index mà ta vừa kiểm tra

```bash
 if (Math.abs(arr[i]) >= Math.abs(arr[largestNumberIndex])) {
      largestNumberIndex = i
    }
```

Bước 3: return largestNumberIndex

Mã giải:

```
const findLargestNumberIndex = (arr: number[]) => {
  let largestNumberIndex = 0

  for (let i = 1; i < arr.length; i++) {
    if (Math.abs(arr[i]) >= Math.abs(arr[largestNumberIndex])) {
      largestNumberIndex = i
    }
  }

  return largestNumberIndex
}
```

### 2. useCallback và useMemo có tác dụng gì ? Khi nào sử dụng các hook đó ?

#### Công dụng:

-   useCallback và useMemo là các hooks áp dụng kỹ thuật memoization, nhận vào hai tham số là một function và dependencies. Có tác dụng hạn chế những lần re-render dư thừa (micro improvements). Có thể kết hợp với React.memo để có được hiêụ suất tốt hơn.
-   ví dụ: Trong một componet ứng dụng React, ta có một đoạn code mất rất nhiều thời gian để xử lý. Nhưng khi chúng ta thay đổi state để render lại component đó thì đoạn code này được xử lý lại mà không có bất kỳ một thay đổi bên trong. Trong trường hợp này chúng ta có thể xử dụng useMemo để cache lại giá trị. Còn trong trường hợp, trong một component con có nhiều props truyền xuống (function, state). Trong trường hợp component cha re-render thì component cũng sẽ bị thay đổi mặc cho những props truyền xuống không hề thay đổi. Trong trường họp này chúng ta sử dụng React.memo để cache lại giá trị cho đến khi props thay đổi. Và lưu ý nếu trong props truyền xuống có function được tạo từ component cha, lúc đó mỗi khi component cha render lại thì function đó (inner function) cũng sẽ được tạo lại, điều đó khiến cho nó được xem là 1 function khác trong khi code không hề thay đổi. Dẫn đến việc nó sẽ khiến components đã cached sẽ bị render lại không cần thiết. Lúc này ta sử dụng useCallback thì vấn để được giải quyết.

#### Khi nào nên sử useCallback và useMemo

-   Chúng ta nên sử dụng hai hook trên khi những trường hợp để cache lại giá trị của một đoạn code xử lý mất nhiều thời gian và không có thay đổi dù cho component có bị re-render. Và để tránh cho những component bị re-render một cách thừa thãi. Nhằm giúp cải thiện về mặt performence cho ứng dụng.

First, run the development server:

### 3.Cho 3 cột To Do, In Progress, Done và các item trong các cột. Mỗi item có tiêu đề, mô tả. Viết source code thỏa các điều kiện sau đây:

### - User có thể chỉnh sửa tiêu đề, mô tả khi ấn vào action "Edit" trong menu (dấu ba chấm dọc) ở mỗi item

### - Cho phép user có thể drag and drop các item qua các cột khác và có thể sắp xếp được trong cùng cột

Open [https://gitlab.com/hoangvu6262/to-do-list](https://gitlab.com/hoangvu6262/to-do-list) to see source code.

Open [Demo](https://to-do-list-jjirp4h0u-hoangvu6262.vercel.app/) with your browser to see the result.
