import { BoardItemType } from '@/configs/const/type'
import { DraggableLocation } from 'react-beautiful-dnd'

export const reorder = <T>(list: T[], startIndex: number, endIndex: number) => {
    const result = Array.from(list)
    const [removed] = result.splice(startIndex, 1)
    result.splice(endIndex, 0, removed)

    return result
}

export const removeByIndex = <T>(list: T[], index: number) => [
    ...list.slice(0, index),
    ...list.slice(index + 1),
]
