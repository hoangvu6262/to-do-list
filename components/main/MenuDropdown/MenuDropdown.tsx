import { useRef } from 'react'
import { Menu, MenuItem, Divider } from '@mui/material'
import GroupAddOutlinedIcon from '@mui/icons-material/GroupAddOutlined'
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined'

type Props = {
    anchorEl: HTMLElement | null
    handleClose: () => void
    handleOpenEditCard: () => void
}

const MenuDropdown = ({ anchorEl, handleClose, handleOpenEditCard }: Props) => {
    const open = Boolean(anchorEl)

    return (
        <>
            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        backgroundColor: 'black',
                        color: '#8e8f8e',
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        padding: '5px 10px',
                        width: 200,
                        '& svg': {
                            width: 19,
                            height: 19,
                        },
                        ' & .MuiMenuItem-root': {
                            width: '100%',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            fontSize: '14px',
                            '&:hover': {
                                backgroundColor: '#3f3f46',
                            },
                        },
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <MenuItem
                    style={{
                        color: '#818cf8',
                    }}
                    onClick={handleOpenEditCard}
                >
                    Edit Card
                    <GroupAddOutlinedIcon
                        style={{
                            color: '#818cf8',
                        }}
                    />
                </MenuItem>

                <MenuItem
                    onClick={handleClose}
                    style={{
                        color: '#f43f5f',
                    }}
                >
                    Delete Card
                    <DeleteOutlineOutlinedIcon
                        style={{
                            color: '#f43f5f',
                        }}
                    />
                </MenuItem>
            </Menu>
        </>
    )
}

export default MenuDropdown
