import { useState, MouseEvent } from 'react'
import { styled, Typography, Paper, IconButton } from '@mui/material'
import MoreVertIcon from '@mui/icons-material/MoreVert'

import { ModalType, BoardItemType } from '@/configs/const/type'
import MenuDropdown from '@/components/main/MenuDropdown/MenuDropdown'
import { TYPE_MODAL } from '@/configs/const/main'

type CardProps = {
    card: BoardItemType
    handleOpenModal: (type: ModalType, data?: BoardItemType) => void
}

const CardContainer = styled(Paper)(({ theme }) => ({
    backgroundColor: '#252e3e',
    ...theme.typography.body2,
    padding: '5px 10px',
    color: '#9da4ae',
    cursor: 'pointer',
}))

const CardHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    gap: 5,
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center',
}))

const Card = ({ card, handleOpenModal }: CardProps) => {
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)

    const handleClick = (event: MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget)
    }

    const handleCloseMenu = () => {
        setAnchorEl(null)
    }

    const handleOpenEditCard = () => {
        handleOpenModal(TYPE_MODAL.EDIT, card)
    }

    return (
        <>
            <CardContainer>
                <CardHeader>
                    <Typography
                        variant="subtitle2"
                        sx={{ fontSize: 15, fontWeight: 500, color: '#fff' }}
                    >
                        {card.name}
                    </Typography>
                    <IconButton onClick={handleClick}>
                        <MoreVertIcon />
                    </IconButton>
                </CardHeader>
                {!!card.description ? (
                    <Typography variant="body2">{card.description}</Typography>
                ) : null}
            </CardContainer>
            <MenuDropdown
                anchorEl={anchorEl}
                handleClose={handleCloseMenu}
                handleOpenEditCard={handleOpenEditCard}
            />
        </>
    )
}

export default Card
