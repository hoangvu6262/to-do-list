import {
    forwardRef,
    useImperativeHandle,
    Ref,
    useState,
    useEffect,
} from 'react'
import useModal from '@/hooks/useModal'
import CustomModal from '@/components/shared/Modal/Modal'
import {
    Button,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    TextField,
} from '@mui/material'
import { FormProvider, useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import * as z from 'zod'

import { TYPE_MODAL } from '@/configs/const/main'
import { ModalType } from '@/configs/const/type'

import { BoardItemType } from '@/configs/const/type'

type CardModalProps = {
    addNewCard: (data: BoardItemType) => void
    updateCard: (data: BoardItemType) => void
    boardId: string
}

export type CardModalRefType = {
    onToggle: (type: ModalType, data?: any) => void
}

const formSchema = z.object({
    name: z.string().min(1, {
        message: 'Title is required.',
    }),
    description: z.string(),
})

const defaultvalues = {
    name: '',
    description: '',
}

const CardModal = (
    { addNewCard, updateCard, boardId }: CardModalProps,
    ref: Ref<CardModalRefType>
) => {
    const [type, setType] = useState<ModalType>(TYPE_MODAL.ADD)
    const { open, data, _handleToggle } = useModal()

    const methods = useForm({
        resolver: zodResolver(formSchema),
        defaultValues: defaultvalues,
    })

    const {
        register,
        handleSubmit,
        reset,
        // formState: { errors, isSubmitting },
    } = methods

    useEffect(() => {
        if (data) {
            methods.setValue('name', data.name)
            methods.setValue('description', data.description)
        }
    }, [data, methods])

    useImperativeHandle(ref, () => ({
        onToggle: handleToggle,
    }))

    const handleToggle = (type: ModalType, data?: BoardItemType) => {
        _handleToggle(data)
        setType(type)
    }

    const title = type === TYPE_MODAL.EDIT ? 'Edit Card' : 'Create a new card'

    const onSubmit = (values: z.infer<typeof formSchema>) => {
        if (type === TYPE_MODAL.ADD) {
            addNewCard({
                name: values.name,
                description: values.description,
                boardId,
            })
        } else {
            updateCard({
                id: data.id,
                name: values.name,
                description: values.description,
                boardId,
            })
        }

        reset()
        _handleToggle()
    }

    return (
        <CustomModal open={open} handleClose={_handleToggle}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                    <FormProvider {...methods}>
                        <form
                            onSubmit={handleSubmit(onSubmit)}
                            encType="multipart/form-data"
                        >
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                {...register('name')}
                                type="text"
                                placeholder="Enter tile"
                                fullWidth
                            />
                            <TextField
                                margin="dense"
                                id="description"
                                multiline
                                minRows={3}
                                maxRows={8}
                                {...register('description')}
                                type="text"
                                placeholder="Enter description"
                                fullWidth
                            />
                            <DialogActions>
                                <Button onClick={handleSubmit(onSubmit)}>
                                    Save
                                </Button>
                                <Button onClick={_handleToggle}>Cancel</Button>
                            </DialogActions>
                        </form>
                    </FormProvider>
                </DialogContentText>
            </DialogContent>
        </CustomModal>
    )
}

export default forwardRef(CardModal)
