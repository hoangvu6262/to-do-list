'use client'

import React, { useRef, useState, ChangeEvent } from 'react'
import { Box, Typography, Stack, Button, TextField } from '@mui/material'
import { styled } from '@mui/material/styles'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import AddIcon from '@mui/icons-material/Add'

import { BoardItemType } from '@/configs/const/type'
import { TYPE_MODAL } from '@/configs/const/main'
import { ModalType } from '@/configs/const/type'
import { useMainContext } from '@/configs/context/provider/MainContext'
import Card from '@/components/main/Card/Card'
import CardModal, {
    CardModalRefType,
} from '@/components/main/CardModal/CardModal'

type BoardProps = {
    id: string
    name: string
    listCard: BoardItemType[]
}

const BoardContainer = styled(Box)(({ theme }) => ({
    padding: theme.spacing(1),
    borderRadius: 10,
    // backgroundColor: '#f1f2f4',
    backgroundColor: '#1c2536',
    boxShadow: 'rgba(0, 0, 0, 0.16) 0px 1px 4px',
    color: '#ffffff',
}))

const BoardHeader = styled('div')(({ theme }) => ({}))

const BoardFooter = styled('div')(({ theme }) => ({
    paddingTop: '10px',
}))

const Board = ({ name, listCard, id }: BoardProps) => {
    const { handleAddNewCard, updateCard, updateBoard } = useMainContext()
    const [title, setTitle] = React.useState(name)
    const [isNameFocused, setIsNamedFocused] = useState(false)
    const cardModalRef = useRef<CardModalRefType>(null)

    const renderListBoardItems = () => {
        return listCard.map((card, index) => (
            <Draggable
                key={card.id}
                index={index}
                draggableId={`draggable-${id}-${card.id}`}
            >
                {(provided, snapshot) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                    >
                        <Card card={card} handleOpenModal={handleOpenModal} />
                    </div>
                )}
            </Draggable>
        ))
    }

    const handleOpenModal = (type: ModalType, data?: BoardItemType) => {
        cardModalRef.current?.onToggle(type, data)
    }

    return (
        <>
            <BoardContainer>
                <BoardHeader>
                    {!isNameFocused ? (
                        <Typography
                            variant="h6"
                            gutterBottom
                            onClick={() => setIsNamedFocused(true)}
                        >
                            {name}
                        </Typography>
                    ) : (
                        <TextField
                            autoFocus
                            size="small"
                            value={title}
                            inputProps={{
                                sx: {
                                    color: '#fff',
                                    paddingLeft: '15px',
                                },
                            }}
                            onChange={(e) => setTitle(e.target.value)}
                            onBlur={() => {
                                setIsNamedFocused(false)
                                updateBoard({
                                    id,
                                    name: title,
                                })
                            }}
                        />
                    )}
                </BoardHeader>
                <Droppable droppableId={id.toString()}>
                    {(provided, snapshot) => (
                        <Stack
                            spacing={1}
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            sx={{ minHeight: 50 }}
                        >
                            {renderListBoardItems()}
                            {provided.placeholder}
                        </Stack>
                    )}
                </Droppable>

                <BoardFooter>
                    <Button
                        startIcon={<AddIcon />}
                        onClick={() => handleOpenModal(TYPE_MODAL.ADD)}
                    >
                        Add a Card
                    </Button>
                </BoardFooter>
            </BoardContainer>
            <CardModal
                ref={cardModalRef}
                addNewCard={handleAddNewCard}
                updateCard={updateCard}
                boardId={id}
            />
        </>
    )
}

export default Board
