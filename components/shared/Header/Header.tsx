import { styled } from '@mui/material/styles'
import { Toolbar, Typography, IconButton } from '@mui/material'
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar'
import MenuIcon from '@mui/icons-material/Menu'

import { SIDEBAR_WIDTH } from '@/configs/const/main'

type HeaderProps = {
    handleDrawerOpen: () => void
    open: boolean
}

interface AppBarProps extends MuiAppBarProps {
    open?: boolean
}

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
    backgroundColor: 'black',
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${SIDEBAR_WIDTH}px)`,
        marginLeft: `${SIDEBAR_WIDTH}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}))

const Header = ({ handleDrawerOpen, open }: HeaderProps) => {
    return (
        <AppBar position="fixed" open={open}>
            <Toolbar>
                {/* <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    sx={{ mr: 2, ...(open && { display: 'none' }) }}
                >
                    <MenuIcon />
                </IconButton> */}
                <Typography variant="h6" noWrap component="div">
                    To Do List
                </Typography>
            </Toolbar>
        </AppBar>
    )
}

export default Header
