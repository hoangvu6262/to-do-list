import Dialog from '@mui/material/Dialog'

type CustomModalProps = {
    open: boolean
    handleClose: () => void
    children: React.ReactNode
}

const CustomModal = ({ open, handleClose, children }: CustomModalProps) => {
    return (
        <Dialog open={open} onClose={handleClose}>
            {children}
        </Dialog>
    )
}

export default CustomModal
