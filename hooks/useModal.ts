import { useState } from 'react'

const useModal = () => {
    const [open, setOpen] = useState<boolean>(false)
    const [data, setData] = useState<any>(null)

    const _handleToggle = (data?: any) => {
        !!data && setData(data)
        setOpen(!open)
    }

    return { open, data, _handleToggle } as const
}

export default useModal
