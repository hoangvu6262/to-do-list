import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import { z } from 'zod'

type FormSchemaType = z.ZodObject<Record<string, any>>

const useFormApp = (formSchema: FormSchemaType, initialValue: any) => {
    const methods = useForm({
        resolver: zodResolver(formSchema),
        defaultValues: initialValue,
    })

    return { methods } as const
}

export default useFormApp
