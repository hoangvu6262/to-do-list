'use client'

import { useState, useEffect } from 'react'
import { Box, Grid, styled } from '@mui/material'
import {
    DragDropContext,
    DropResult,
    DraggableLocation,
} from 'react-beautiful-dnd'

import { useMainContext } from '@/configs/context/provider/MainContext'
import { BoardItemType } from '@/configs/const/type'
import { reorder } from '@/helpers/cardHelper'
import Board from '@/components/main/Board/Board'

type BoardProps = {}

const BoardsContainer = styled('div')(() => ({
    display: 'flex',
    gap: 15,
    overflowX: 'scroll',
}))

const BoardContainer = styled('div')(() => ({
    width: 280,
}))

const ButtonAddBoard = styled('div')(() => ({
    cursor: 'pointer',
    padding: '10px 15px',
    backgroundColor: '#ffffff3d',
    width: '100%',
    borderRadius: 10,
    color: '#9c9fa2',
}))

const Boards = (props: BoardProps) => {
    const { listBoards, listCards, reorderListCard, addNewBoard } =
        useMainContext()
    const [ready, setReady] = useState(false)

    useEffect(() => {
        if (process.browser) {
            setReady(true)
        }
    }, [])

    const renderListBoards = () => {
        return listBoards.map((board) => {
            return (
                <BoardContainer key={board.id}>
                    <Board
                        {...board}
                        listCard={listCards.filter(
                            (item) => item.boardId === board.id
                        )}
                    />
                </BoardContainer>
            )
        })
    }

    const onDragEnd = (result: DropResult) => {
        const { source, destination } = result

        // if the user drops outside of a droppable destination
        if (!destination) return

        // If the user drags and drops back in the same position
        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ) {
            return
        }

        if (source.droppableId === destination.droppableId) {
            moveRowSameBoard(source, destination)
        } else {
            moveRowDifferentBoard(source, destination)
        }
    }

    const moveRowSameBoard = (
        source: DraggableLocation,
        destination: DraggableLocation
    ) => {
        // Items are in the same list, so just re-order the list array
        const destinationRow = listCards.filter(
            (item: BoardItemType) => item.boardId === destination.droppableId
        )
        const reordered: any = reorder<BoardItemType>(
            [...destinationRow],
            source.index,
            destination.index
        )

        // Get rid of old list and replace with updated one
        const filteredCards = listCards.filter(
            (card: BoardItemType) => card.boardId !== source.droppableId
        )

        reorderListCard([...filteredCards, ...reordered])
    }

    const moveRowDifferentBoard = (
        source: DraggableLocation,
        destination: DraggableLocation
    ) => {
        // extract the tasks from the columnn
        const sourceRow = listCards.filter(
            (item) => item.boardId === source.droppableId
        )
        const destinationRow = listCards.filter(
            (item) => item.boardId === destination.droppableId
        )

        // remove the source item
        const [removed] = sourceRow.splice(source.index, 1)
        // insert the source item at the new colIndex
        destinationRow.splice(destination.index, 0, {
            ...removed,
            boardId: destination.droppableId,
        })

        const filteredCards = listCards.filter(
            (card: BoardItemType) =>
                card.boardId !== source.droppableId &&
                card.boardId !== destination.droppableId
        )

        reorderListCard([...filteredCards, ...destinationRow, ...sourceRow])
    }

    return (
        <>
            {ready && (
                <DragDropContext onDragEnd={onDragEnd}>
                    <BoardsContainer>
                        {renderListBoards()}
                        <BoardContainer>
                            <ButtonAddBoard onClick={addNewBoard}>
                                + Add another board
                            </ButtonAddBoard>
                        </BoardContainer>
                    </BoardsContainer>
                </DragDropContext>
            )}
        </>
    )
}

export default Boards
