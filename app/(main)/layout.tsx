'use client'

import * as React from 'react'
import { styled } from '@mui/material/styles'
import { Box, CssBaseline } from '@mui/material'

// import { SIDEBAR_WIDTH } from '@/configs/const/main'
import Header from '@/components/shared/Header/Header'
// import SideBar from '@/components/shared/SideBar/SideBar'
import MainProvider from '@/configs/context/provider/MainContext'

type MainLayoutProps = {
    children: React.ReactNode
}

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
    open?: boolean
}>(({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    // marginLeft: `-${SIDEBAR_WIDTH}px`,
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    }),
}))

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}))

const MainLayout = ({ children }: MainLayoutProps) => {
    const [open, setOpen] = React.useState(false)

    const handleDrawerOpen = () => {
        setOpen(true)
    }

    // const handleDrawerClose = () => {
    //     setOpen(false)
    // }

    return (
        <MainProvider>
            <Box sx={{ display: 'flex' }}>
                {/* <CssBaseline /> */}
                <Header handleDrawerOpen={handleDrawerOpen} open={open} />
                {/* <SideBar handleDrawerClose={handleDrawerClose} open={open} /> */}
                <Main
                    open={open}
                    sx={{ backgroundColor: '#0e1320', height: '100vh' }}
                >
                    <DrawerHeader />
                    {children}
                </Main>
            </Box>
        </MainProvider>
    )
}

export default MainLayout
