import { v1 as uuidv1 } from 'uuid'
import BoardIcon from '@/assets/icon/BoardIcon'
import TableIcon from '@/assets/icon/TableIcon'

import {
    SidebarType,
    BoardItemType,
    BoardType,
    AddType,
    DeleteType,
    EditType,
} from '@/configs/const/type'

export const TYPE_MODAL: {
    ADD: AddType
    EDIT: EditType
    DELETE: DeleteType
} = {
    ADD: 'Add',
    EDIT: 'Edit',
    DELETE: 'Delete',
}

export const SIDEBAR_WIDTH: number = 240

export const LIST_SIDEBAR: SidebarType[] = [
    {
        id: uuidv1(),
        name: 'Dashboard',
        Icon: BoardIcon,
    },
    {
        id: uuidv1(),
        name: 'Board',
        Icon: TableIcon,
    },
]

export const LIST_BOARD: BoardType[] = [
    {
        id: 'id1',
        name: 'To Do',
    },
    {
        id: 'id2',
        name: 'In Progress',
    },
    {
        id: 'id3',
        name: 'Done',
    },
]

export const LIST_CARD: BoardItemType[] = [
    {
        id: uuidv1(),
        name: 'Practice 2',
        boardId: 'id1',
        description: '',
    },
    {
        id: uuidv1(),
        name: 'Check task 4',
        boardId: 'id1',
        description: 'If the task 4 has error, go fixed that.',
    },
    {
        id: uuidv1(),
        name: 'Practice',
        boardId: 'id2',
        description: '',
    },
    {
        id: uuidv1(),
        name: 'Check task',
        boardId: 'id1',
        description: 'If the task has error, go fixed that.',
    },
    {
        id: uuidv1(),
        name: 'Practice 1',
        boardId: 'id1',
        description: '',
    },
    {
        id: uuidv1(),
        name: 'Check task 1',
        boardId: 'id3',
        description: 'If the task 1 has error, go fixed that.',
    },
]
