export type SidebarType = {
    id: string
    name: string
    Icon: () => JSX.Element
}

export type BoardItemType = {
    id?: string
    name: string
    description: string
    boardId: string
}

export type BoardType = {
    id: string
    name: string
}

export type AddType = 'Add'
export type EditType = 'Edit'
export type DeleteType = 'Delete'

export type ModalType = AddType | EditType | EditType
