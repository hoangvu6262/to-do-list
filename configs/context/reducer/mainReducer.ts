import { MainType } from '@/configs/context/provider/MainContext'
import { v1 as uuidv1 } from 'uuid'

const mainReducer = (
    state: MainType,
    action: { type: string; payload: any }
) => {
    switch (action.type) {
        case 'SET_DEFAULT_LIST_BOARD':
            return { ...state, listBoards: action.payload }
        case 'ADD_NEW_BOARD': {
            return {
                ...state,
                listBoards: [...state.listBoards, action.payload],
            }
        }
        case 'ADD_NEW_CARD': {
            const newState = { ...state }
            const { listCards } = newState

            return {
                ...newState,
                listCards: [...listCards, { id: uuidv1(), ...action.payload }],
            }
        }
        case 'UPDATE_CARD': {
            const newState = { ...state }
            const { listCards } = newState
            const data = action.payload

            const newListCards = listCards.map((card) => {
                if (card.id === data.id) {
                    return data
                }

                return card
            })

            return { ...newState, listCards: newListCards }
        }

        case 'REORDER_CARD': {
            const newState = { ...state }

            return {
                ...newState,
                listCards: action.payload,
            }
        }
        case 'UPDATE_BOARD': {
            const newState = { ...state }
            const data = action.payload
            const newListBoards = newState.listBoards.map((board) => {
                if (data.id === board.id) {
                    return data
                }

                return board
            })

            return { ...newState, listBoards: newListBoards }
        }

        default:
            throw new Error('invalid action')
    }
}

export default mainReducer
