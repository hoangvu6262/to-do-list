'use client'

import React, { useReducer, useContext } from 'react'

import { BoardType, BoardItemType } from '@/configs/const/type'
import { LIST_BOARD, LIST_CARD } from '@/configs/const/main'
import mainReducer from '@/configs/context/reducer/mainReducer'

export type MainType = {
    listBoards: BoardType[]
    listCards: BoardItemType[]
}

type Functiontype = {
    handleAddNewCard: (data: BoardItemType) => void
    updateCard: (data: BoardItemType) => void
    reorderListCard: (newListCards: BoardItemType[]) => void
    addNewBoard: () => void
    updateBoard: (_data: BoardType) => void
}

const initialState = {
    listBoards: LIST_BOARD,
    listCards: LIST_CARD,
}

export const MainContext = React.createContext<MainType & Functiontype>({
    ...initialState,
    handleAddNewCard: (_data: BoardItemType) => {},
    updateCard: (_data: BoardItemType) => {},
    reorderListCard: (_newListCards: BoardItemType[]) => {},
    addNewBoard: () => {},
    updateBoard: (_data: BoardType) => {},
})

export const useMainContext = () => {
    return useContext(MainContext)
}

const MainProvider = ({ children }: { children: React.ReactNode }) => {
    const [mainState, dispatchMainState] = useReducer(mainReducer, initialState)

    const handleAddNewCard = (data: BoardItemType) => {
        dispatchMainState({
            type: 'ADD_NEW_CARD',
            payload: data,
        })
    }

    const updateCard = (data: BoardItemType) => {
        dispatchMainState({
            type: 'UPDATE_CARD',
            payload: data,
        })
    }

    const reorderListCard = (newListCards: BoardItemType[]) => {
        dispatchMainState({
            type: 'REORDER_CARD',
            payload: newListCards,
        })
    }

    const addNewBoard = () => {
        dispatchMainState({
            type: 'ADD_NEW_BOARD',
            payload: {
                id: `id${mainState.listBoards.length + 1}`,
                name: 'New Board',
            },
        })
    }

    const updateBoard = (data: BoardType) => {
        dispatchMainState({
            type: 'UPDATE_BOARD',
            payload: data,
        })
    }

    return (
        <MainContext.Provider
            value={{
                ...mainState,
                handleAddNewCard,
                updateCard,
                reorderListCard,
                addNewBoard,
                updateBoard,
            }}
        >
            {children}
        </MainContext.Provider>
    )
}

export default MainProvider
